import { Container } from "space-mvc"
import { HomeController } from "./controller/home-controller"

const container = new Container()

//Bind our home controller
container.bind(HomeController).toSelf().inSingletonScope()

//Bind the service and DAO for person

export {
    container
}
import { StoreDefinition } from "space-mvc";

let storeDefinitions: StoreDefinition[] = [{
    name: "student", 
    type: "mfsstore", 
    load: 100, 
    options: {
        schema: {
            walletAddress: { unique: true },
            firstName: { unique: false },
            lastName: { unique: false }
        }
    }

}]

export {
    storeDefinitions
}
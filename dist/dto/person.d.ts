interface Person {
    id?: string;
    firstName: string;
    lastName: string;
}
export { Person };
